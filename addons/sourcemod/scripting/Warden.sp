#pragma semicolon 1
//untest

#define PLUGIN_VERSION "1.2"
#define PLUGIN_PREFIX "[\x06Warden\x01]"

#include <emitsoundany>
#include <basecomm>

#include <sourcemod>

public Plugin myinfo = 
{
	name = "Warden Revamped",
	author = "Oscar Wos (OSWO)",
	description = "Hosties - Force Warden",
	version = PLUGIN_VERSION,
	url = "www.tangoworldwide.net",
};

bool g_Enabled = true;
bool g_ActiveWarden = false;
int g_WardenId = -1;
Handle g_DeadTimer = INVALID_HANDLE;
Handle g_ResetLock = INVALID_HANDLE;

int g_ClientPressed[MAXPLAYERS];

char wardenName[MAX_NAME_LENGTH];

char s_wardenTake[128] = "tango/takeWarden.mp3";
char s_wardenGone[128] = "tango/goneWarden.mp3";

public void OnPluginStart()
{
	CreateNative("warden_iswarden", native_iswarden);
	CreateNative("warden_exist", native_exist);
	LoadTranslations("common.phrases");
	
	RegAdminCmd("sm_w", command_warden, 0, "");
	RegAdminCmd("sm_uw", command_unwarden, 0, "");
	RegAdminCmd("sm_rw", command_resetwarden, ADMFLAG_CHAT, "");
	RegAdminCmd("sm_fw", command_forcewarden, ADMFLAG_CHAT, "");
	
	HookEvent("round_start", event_roundStart);
	HookEvent("player_death", event_playerDeath);
	HookEvent("round_end", event_roundEnd);
	
	HookEvent("round_poststart", event_postStart);
	
	CreateTimer(1.0, lowerPressed, _, TIMER_REPEAT);
}

public native_iswarden(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	
	if(!IsClientInGame(client) && !IsClientConnected(client))
		ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", client);
		
	if(client == g_WardenId)
		return true;
	
	return false;
}

public native_exist(Handle plugin, int numParams)
{
	if(g_WardenId != -1)
		return true;
		
	return false;
}


public OnMapStart()
{
	char buffer[128];
	PrecacheSoundAny(s_wardenTake);
	PrecacheSoundAny(s_wardenGone);
	
	Format(buffer, sizeof(buffer), "sound/%s", s_wardenTake);
	AddFileToDownloadsTable(buffer);
	
	Format(buffer, sizeof(buffer), "sound/%s", s_wardenGone);
	AddFileToDownloadsTable(buffer);
	
	if (g_WardenId != -1)
	{
		UnSetWarden();
	}
}

public Action event_roundStart(Handle event, char[] name, bool dontBroadcast)
{
	g_Enabled = true;
	
	g_ResetLock = CreateTimer(3.0, ResetLock);
}

public Action ResetLock(Handle timer)
{
	if (g_ResetLock != INVALID_HANDLE)
	{
		KillTimer(g_ResetLock);
		g_ResetLock = INVALID_HANDLE;
	}
}

public Action event_roundEnd(Handle event, char[] name, bool dontBroadcast)
{
	if (g_WardenId != -1)
	{
		UnSetWarden();
	}
	
	killTimer();
}

public Action event_postStart(Handle event, char[] name, bool dontBroadcast)
{
	killTimer();
	
	int numT = GetTeamClientCount(2);
	int numCT = GetTeamClientCount(3);
	
	if (numCT * 2 > numT)
	{
		if (g_WardenId != -1)
		{
			UnSetWarden();
		}
		
		PrintToChatAll("%s The ratio of CT to T is; %i:%i - It's a \x07Ratio Freeday!", PLUGIN_PREFIX, numCT, numT);
		g_Enabled = false;
	}
}

public Action event_playerDeath(Handle event, char[] name, bool dontBroadcast)
{
	int client = GetClientOfUserId(GetEventInt(event, "userid"));
	
	if (client == g_WardenId)
	{
		g_ActiveWarden = false;
		g_WardenId = -1;
		
		for (int i = 1; i < MaxClients; i++)
		{
			if (IsValidPlayer(i))
			{
				if (GetClientTeam(i) == 3)
					PrintToChat(i, "%s Warden Has Died! You have 7 seconds to take warden via: \x07!w", PLUGIN_PREFIX);
				else
					PrintToChat(i, "%s Warden Has Died! CT's have 7 seconds to reclaim warden!", PLUGIN_PREFIX);
					
			}
		}
		
		PlayGoneWardenSound();
		
		g_DeadTimer = CreateTimer(7.0, deadWardenCheck);
	}	
}

public Action lowerPressed(Handle timer)
{
	for (int i = 1; i < MaxClients; i++)
	{
		if (IsValidPlayer(i) && g_ClientPressed[i] >= 1)
		{
			g_ClientPressed[i] = g_ClientPressed[i] - 1;
		}
	}
}

public Action deadWardenCheck(Handle timer)
{
	if (g_ActiveWarden == false)
	{
		g_Enabled = false;
		PrintToChatAll("%s It's now a \x07Freeday!", PLUGIN_PREFIX);
	}
	
	killTimer();
}

public Action command_unwarden(int client, int args)
{
	if (client == g_WardenId)
	{
		UnSetWarden();
		PrintToChatAll("%s The Warden has retired!", PLUGIN_PREFIX);
		PlayGoneWardenSound();
	} else
		PrintToChat(client, "%s You're not the Warden!", PLUGIN_PREFIX);
	
	return Plugin_Handled;
}

public Action command_warden(int client, int args)
{	
	if (g_Enabled)
	{
		if (GetClientTeam(client) == 3)
		{
			g_ClientPressed[client]++;
			
			if (IsPlayerAlive(client))
			{
				if (!g_ActiveWarden)
				{
					if (g_ClientPressed[client] > 1)
					{
						char clientName[MAX_NAME_LENGTH];
						GetClientName(client, clientName, sizeof(clientName));
							
						char clientSteam[64];
						GetClientAuthId(client, AuthId_Engine, clientSteam, sizeof(clientSteam));
						
						LogToFileEx("addons/sourcemod/logs/Warden/spam.txt", "Player: %s has used a spam bind! SteamID - %s", clientName, clientSteam);
						
						PrintToChat(client, "%s You've been denied Warden due to your Warden bind. Try again in %i seconds", PLUGIN_PREFIX, g_ClientPressed[client]);
					} else {
						SetWarden(client);
						killTimer();
						
						GetWardenName();
						PrintToChatAll("%s %s is now the Warden!", PLUGIN_PREFIX, wardenName);
						PlayTakeWardenSound();
					}
				} else {
					if (g_ClientPressed[client] > 1)
					{
						char clientName[MAX_NAME_LENGTH];
						GetClientName(client, clientName, sizeof(clientName));
							
						char clientSteam[64];
						GetClientAuthId(client, AuthId_Engine, clientSteam, sizeof(clientSteam));
							
						if (client == g_WardenId)
						{
							PrintToChatAll("%s \x02%s just used a spam bind - They've been Un-Wardened", PLUGIN_PREFIX, clientName);
							PrintToChatAll("%s \x02%s just used a spam bind - They've been Un-Wardened", PLUGIN_PREFIX, clientName);
							PrintToChatAll("%s \x02%s just used a spam bind - They've been Un-Wardened", PLUGIN_PREFIX, clientName);
							UnSetWarden();
							PlayGoneWardenSound();
							
							LogToFileEx("addons/sourcemod/logs/Warden/spam.txt", "Player: %s has used a spam bind! SteamID - %s", clientName, clientSteam);
						} else
							PrintToChat(client, "%s \x07You should stop spamming that bind...", PLUGIN_PREFIX);
					} else
						PrintWardenName(client);
				}
			} else
				PrintWardenName(client);
		} else
			PrintWardenName(client);
	} else {
		PrintToChat(client, "%s There cannot be a Warden right now!", PLUGIN_PREFIX);
	}
		
	return Plugin_Handled;
}

public PrintWardenName(int client)
{
	if (g_ActiveWarden)
	{
		if (g_WardenId != -1)
		{
			GetWardenName();
			PrintToChat(client, "%s Warden is: %s", PLUGIN_PREFIX, wardenName);
		} else
			PrintToChat(client, "%s Error with Warden! Wait till next round!", PLUGIN_PREFIX);
	} else
		PrintToChat(client, "%s Warden has not been taken!", PLUGIN_PREFIX);
}

public Action command_resetwarden(int client, int args)
{	
	if (g_Enabled == true)
	{
		if (g_ActiveWarden == true)
		{
			if (CanUserTarget(client, g_WardenId))
			{
				GetWardenName();
				ShowActivity2(client, "[\x06Warden\x01] \x0D", "\x01Fired \x07%s \x01- The Warden!", wardenName);
				PlayGoneWardenSound();
				
				UnSetWarden();	
			} else
				PrintToChat(client, "%s You cannot target that warden!", PLUGIN_PREFIX);
		} else
			PrintToChat(client, "%s There's no Warden!", PLUGIN_PREFIX);
	} else
		PrintToChat(client, "%s No one can be Warden currently!", PLUGIN_PREFIX);
	
	return Plugin_Handled;	
}

public Action command_forcewarden(int client, int args)
{
	if (args == 1)
	{
		char arg1[256];
		GetCmdArg(1, arg1, sizeof(arg1));
		
		int target = FindTarget(client, arg1);
		
		if (!(target <= 0))
		{
			if (IsPlayerAlive(target))
			{
				if (GetClientTeam(target) == 3)
				{
					if (g_Enabled)
					{
						char targetName[MAX_NAME_LENGTH];
						GetClientName(target, targetName, sizeof(targetName));
						
						if (g_ActiveWarden)
						{
							if (CanUserTarget(client, g_WardenId))
							{
								UnSetWarden();
								SetWarden(target);
								PlayTakeWardenSound();
								
								ShowActivity2(client, "[\x06Warden\x01] \x0D", "\x01Set Warden to \x07%s", targetName);
							} else
								PrintToChat(client, "%s You cannot target the current Warden!", PLUGIN_PREFIX);
								
						} else {
							SetWarden(target);
							PlayTakeWardenSound();
							
							ShowActivity2(client, "[\x06Warden\x01] \x0D", "\x01Set Warden to \x07%s", targetName);
						}
							
					} else
						PrintToChat(client, "%s There cannot be a Warden right now! It's either a Ratio FD or an error has occured!", PLUGIN_PREFIX);
				} else
					PrintToChat(client, "%s Target is on wrong team!", PLUGIN_PREFIX);
			} else
				PrintToChat(client, "%s Target is dead!", PLUGIN_PREFIX);
		}
	} else
		PrintToChat(client, "%s \x07Usage: \x10sm_fw \x01<player>", PLUGIN_PREFIX);
	
	return Plugin_Handled;
}

public UnSetWarden()
{
	SetEntityRenderColor(g_WardenId, 255, 255, 255, 255);
	
	g_ActiveWarden = false;
	g_WardenId = -1;
}

public SetWarden(int client)
{
	SetEntityRenderColor(client, 0, 0, 255, 255);
	BaseComm_SetClientMute(client, false);
	
	g_ActiveWarden = true;
	g_WardenId = client;
}

public GetWardenName()
{
	GetClientName(g_WardenId, wardenName, sizeof(wardenName));
}

public killTimer()
{
	if (g_DeadTimer != INVALID_HANDLE)
	{
		KillTimer(g_DeadTimer);
		g_DeadTimer = INVALID_HANDLE;
	}
}

public PlayTakeWardenSound()
{
	for (int i = 0; i < MaxClients; i++)
	{
		if (IsValidPlayer(i))
			EmitSoundToClientAny(i, "tango/takeWarden.mp3");	
	}
}

public PlayGoneWardenSound()
{
	for (int i = 0; i < MaxClients; i++)
	{
		if (IsValidPlayer(i))
			EmitSoundToClientAny(i, "tango/goneWarden.mp3");	
	}
}

stock bool IsValidPlayer(int client, bool alive = false)
{
   if(client >= 1 && client <= MaxClients && IsClientConnected(client) && IsClientInGame(client) && (alive == false || IsPlayerAlive(client)))
   {
       return true;
   }
   return false;
}